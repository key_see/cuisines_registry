# Cuisines Registry

## The story:

Cuisines Registry is an important part of Book-That-Table Inc. backend stack. It keeps in memory customer preferences for restaurant cuisines and is accessed by a bunch of components to register and retrieve data. 


The first iteration of this component was implemented by rather inexperienced developer and now may require some cleaning while new functionality is being added. But fortunately, according to his words: "Everything should work and please keep the test coverage as high as I did"


## Your tasks:
1. **[Important!]** Adhere to the boy scout rule. Leave your code better than you found it.
It is ok to change any code as long as the CuisinesRegistry interface remains unchanged.
2. Make is possible for customers to follow more than one cuisine (return multiple cuisines in de.quandoo.recruitment.registry.api.CuisinesRegistry#customerCuisines)
3. Implement de.quandoo.recruitment.registry.api.CuisinesRegistry#topCuisines - returning list of most popular (highest number of registered customers) ones
4. Create a short write up on how you would plan to scale this component to be able process in-memory billions of customers and millions of cuisines (Book-That-Table is already planning for galactic scale). (100 words max)

## Submitting your solution

+ Fork it to a private gitlab repository.
+ Put write up mentioned in point 4. into this file.
+ Send us a link to the repository. together with private ssh key that allows access (settings > repository > deploy keys - **you need to generate a new key pair just for this**). **Please be careful about this** as it saves us a lot of time if done properly - We need an **unencrypted private key**; the public one you set in gitlab deploy keys settings.


SOLUTION OF 4

To improuve In-memory database (without using out of the box RDBMS-database) and save memory resource it's better to use
boolean 2D matrix. Y-scale for customers, X -scale for cuisines, and set up 1 if customer prefer that cuisine,
so every new customer would require just numberOfCuisines/8 bytes, every new cuisine would require numberOfCustomers/8 bytes
For example:

          |FRENCH | GERMAN | ITALIAN
          |__________________________
Customer1 |   0       1         1
Customer2 |   1       1         0
Customer3 |   0       0         1


Top cuisines list can be cashed and updated only on writing operations

For stability and consistency issues can be used clustering, for example solutions within RDBMS databases