package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;
import java.util.stream.Collectors;


public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private Map<Cuisine, Set<Customer>> cuisinesCustomersMap = new HashMap<>();

    @Override
    public void register(final Customer customer, final Cuisine cuisine) {
        checkCuisineParameter(cuisine);
        checkCustomerParameter(customer);
        Set<Customer> customers = cuisinesCustomersMap.get(cuisine);
        if (customers == null) {
            customers = new HashSet<>();
        }
        customers.add(customer);
        cuisinesCustomersMap.put(cuisine, customers);
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        checkCuisineParameter(cuisine);
        return new ArrayList<>(cuisinesCustomersMap.get(cuisine));
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        checkCustomerParameter(customer);
        List<Cuisine> cuisines = new ArrayList<>();
        for(Map.Entry<Cuisine, Set<Customer>> entry : cuisinesCustomersMap.entrySet()) {
            Set<Customer> customersPerCuisine = entry.getValue();
            if (customersPerCuisine != null && customersPerCuisine.contains(customer))
                cuisines.add(entry.getKey());
        }
        return cuisines;
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
        ArrayList <Cuisine> topCuisines = new ArrayList<>(cuisinesCustomersMap.keySet());
        return topCuisines
                .stream()
                .sorted((f1, f2) -> Long.compare(cuisinesCustomersMap.get(f2).size(), cuisinesCustomersMap.get(f1).size()))
                .limit(n)
                .collect(Collectors.toList());
    }

    private void checkCuisineParameter(Cuisine cuisine) {
        if (cuisine == null) {
            throw new IllegalArgumentException("Cuisine was not specified");
        }
    }

    private void checkCustomerParameter(Customer customer) {
        if (customer == null) {
            throw new IllegalArgumentException("Customer was not specified");
        }
    }
}
