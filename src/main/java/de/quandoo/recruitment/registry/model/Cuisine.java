package de.quandoo.recruitment.registry.model;

public enum Cuisine {
    ITALIAN("italian"), GERMAN("german"), FRENCH("french");

    Cuisine(final String name) {
        this.name = name;
    }

    private final String name;

    public String getName() {
        return name;
    }
}