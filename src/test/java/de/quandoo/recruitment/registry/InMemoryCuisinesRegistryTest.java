package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class InMemoryCuisinesRegistryTest {

    private static CuisinesRegistry cuisinesRegistry;

    @Test
    public void testGetCustomers() {
        cuisinesRegistry = getCuisinesRegistry();
        setupCuisinesRegistryTestValues();
        assertEquals(cuisinesRegistry.cuisineCustomers(Cuisine.FRENCH).get(0).getUuid(), "1");
    }

    @Test
    public void testGetCuisines() {
        cuisinesRegistry = getCuisinesRegistry();
        setupCuisinesRegistryTestValues();
        assertEquals(cuisinesRegistry.customerCuisines(new Customer("3")), Collections.singletonList(Cuisine.ITALIAN));
    }

    @Test(expected = RuntimeException.class)
    public void testRegisterNullCustomer() {
        cuisinesRegistry = getCuisinesRegistry();
        cuisinesRegistry.register(null,  Cuisine.FRENCH);
    }

    @Test(expected = RuntimeException.class)
    public void testGetCustomersNP() {
        cuisinesRegistry = getCuisinesRegistry();
        cuisinesRegistry.cuisineCustomers(null);
    }

    @Test(expected = RuntimeException.class)
    public void testGetCuisinesNP() {
        cuisinesRegistry = getCuisinesRegistry();
        cuisinesRegistry.customerCuisines(null);
    }

    @Test
    public void testTopCuisines() {
        cuisinesRegistry = getCuisinesRegistry();
        cuisinesRegistry.register(new Customer("customer1"), Cuisine.FRENCH);
        cuisinesRegistry.register(new Customer("customer2"), Cuisine.FRENCH);
        cuisinesRegistry.register(new Customer("customer3"), Cuisine.ITALIAN);

        List<Cuisine> expectedList = Arrays.asList(Cuisine.FRENCH, Cuisine.ITALIAN);

        assertEquals(expectedList, cuisinesRegistry.topCuisines(2));
    }

    private CuisinesRegistry getCuisinesRegistry(){
        return new InMemoryCuisinesRegistry();
    }

    private void setupCuisinesRegistryTestValues(){
        cuisinesRegistry = getCuisinesRegistry();
        cuisinesRegistry.register(new Customer("1"),  Cuisine.FRENCH);
        cuisinesRegistry.register(new Customer("2"),  Cuisine.GERMAN);
        cuisinesRegistry.register(new Customer("3"),  Cuisine.ITALIAN);
    }
}